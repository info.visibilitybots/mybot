import glob
import os
from pathlib import Path
from typing import Optional
import uvicorn
from dotenv import load_dotenv
from fastapi import (
    FastAPI, Request, UploadFile,
    HTTPException, BackgroundTasks
)
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from schemas.response import CompletionResponse, GenericResponse
from schemas.question import Question, File
from core.logger import logger
from clark.helpers import get_chain, create_vectors

from fastapi import FastAPI, HTTPException, status, Depends
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from passlib.context import CryptContext
from pymongo import MongoClient
from pymongo.collection import Collection
from pydantic import BaseModel
import os

app = FastAPI()
security = HTTPBasic()
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

from pymongo import MongoClient

client = MongoClient("mongodb+srv://raziullah0316:LFpwAAtHVxMczZ9K@unibot.s7ch0ps.mongodb.net/")
db = client["unibot"]  # Replace with your database name
collection = db["users"]  # Replace with your collection name



# # MongoDB setup
# client = MongoClient("mongodb://resume:iyPQ48NbwL3OPTQx@resume.dize47g.mongodb.net:27017/")
# db = client["resume"]
# collection: Collection = db["users"]


app = FastAPI()

from fastapi.middleware.cors import CORSMiddleware

# Define CORS settings
origins = ["*"]  # Allow requests from any origin

# Add CORS middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE", "OPTIONS"],
    allow_headers=["*"],
)

# Assuming the "static" directory is in the same directory as your main script
app.mount("/static", StaticFiles(directory="static"), name="static")

# app.mount("static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="static")

load_dotenv()

class User(BaseModel):
    username: str
    password: str

def get_user_collection():
    return collection

def get_password_hash(password):
    return pwd_context.hash(password)

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

# @app.post("/")
@app.post("/register", tags=["Auth"])
async def register_user(user: User, collection: Collection = Depends(get_user_collection)):
    user_in_db = collection.find_one({"username": user.username})
    if user_in_db:
        raise HTTPException(status_code=400, detail="Username already registered")

    hashed_password = get_password_hash(user.password)
    user_dict = user.dict()
    user_dict.update({"password": hashed_password})
    collection.insert_one(user_dict)
    return {"username": user.username, "message": "User registered successfully"}

@app.post("/login", tags=["Auth"])
async def login(credentials: HTTPBasicCredentials = Depends(security), collection: Collection = Depends(get_user_collection)):
    user_in_db = collection.find_one({"username": credentials.username})
    if not user_in_db:
        raise HTTPException(status_code=400, detail="Incorrect username or password")

    if not verify_password(credentials.password, user_in_db["password"]):
        raise HTTPException(status_code=400, detail="Incorrect username or password")

    return {"username": credentials.username, "message": "Login successful"}


@app.post("/process/", tags=["LLM"])
async def process_files(background_tasks: BackgroundTasks) -> GenericResponse:
    background_tasks.add_task(create_vectors)
    logger.info("Processing files")
    return GenericResponse(status="success")


@app.post("/completions/", tags=["LLM"])
async def completions(question: Question) -> CompletionResponse:
    chain = get_chain()
    content = chain.run(question.message)
    logger.info(f"Response: {content}")
    return CompletionResponse(content=content)


if __name__ == "__main__":
    uvicorn.run("app:app", host="127.0.0.1", port=3000, reload=True)
